from django.shortcuts import render, redirect
from .models import Matkul as mk
from .forms import MatKulForm

def home(request):
    return render(request, 'main/home.html')

def profile(request):
    return render (request, 'main/profile.html')

def experience(request):
    return render (request, 'main/experience.html')

def achievement(request):
    return render (request, 'main/achievement.html')

def projects(request):
    return render (request, 'main/projects.html')

def story1(request):
    return render (request, 'main/story1.html')

def pelajaran(request):
    form = MatKulForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            stud = mk()
            stud.Name = form.cleaned_data['Name']
            stud.NameDos = form.cleaned_data['NameDos']
            stud.Sks = form.cleaned_data['Sks']
            stud.Desc = form.cleaned_data['Desc']
            stud.Year = form.cleaned_data['Year']
            stud.Room = form.cleaned_data['Room']
            stud.save()
            return redirect('main/matkul')
    stud = mk.objects.all()
    form = MatKulForm()
    response = {"stud":stud, 'form':form}
    return render(request,'main/matkul.html', response)

def mk_delete(request, pk):
    form = MatKulForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            stud = mk()
            stud.Name = form.cleaned_data['Name']
            stud.NameDos = form.cleaned_data['NameDos']
            stud.Sks = form.cleaned_data['Sks']
            stud.Desc = form.cleaned_data['Desc']
            stud.Year = form.cleaned_data['Year']
            stud.Room = form.cleaned_data['Room']
            stud.save()
        return redirect('main/matkul')
    else:
        mk.objects.filter(pk=pk).delete()
        data = mk.objects.all()
        form = MatKulForm()
        response = {"stud":data, 'form':form}
        return render(request,'main/matkul.html', response)