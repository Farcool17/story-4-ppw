from django import forms
from .models import Matkul

class MatKulForm(forms.Form):
    Name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Nama',
        'type' : 'text',
        'required' : True,
    }))

    NameDos = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Nama Dosen',
        'type' : 'text',
        'required' : True,
    }))

    Sks = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Jumlah SKS',
        'type' : 'text',
        'required' : True,
    }))

    Desc = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Deskripsi',
        'type' : 'text',
        'required' : True,
    }))

    Year = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Semester Tahun',
        'type' : 'text',
        'required' : True,
    }))

    Room = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder' : 'Kelas',
        'type' : 'text',
        'required' : True,
    }))