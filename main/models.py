from django.db import models

# Create your models here.
class Matkul(models.Model):
    Name = models.TextField(max_length=50)
    NameDos = models.TextField(max_length=50)
    Sks = models.TextField(max_length=50)
    Desc = models.TextField(max_length=50)
    Year = models.TextField(max_length=50)
    Room = models.TextField(max_length=50)

